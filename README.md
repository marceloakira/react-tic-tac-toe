# Code from tutorial tic-tac-toe
Based on React tutorial, with some modifications:
* Use of hooks
* Testing with Storybook

Slides avalaible at:
* [pt-br](http://marceloakira.gitlab.io/tutorial/frontend-com-react-jogo-da-velha-slides.html#/)
* en: soon
