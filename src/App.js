import React from 'react';
import Board from './Board';

function App() {
  return (
      <Board      
        squares={[ null, null, null, 
          null, null, null, 
          null, null, null
        ]}
      />
  );
}

export default App;
