import React, { useEffect, useRef } from 'react'
import './Game.css'

function Square(props) { 
  
  const canvasRef = useRef()

  const handleClick = () => {
    // alert('click from Square of id: '+props.id)
    props.handleClick(props.id)
  }

  useEffect(() => {
    const ctx = canvasRef.current.getContext('2d')
    ctx.font = '25px arial'
    console.log('props.value: '+props.value)
    if ( props.value === 'x' ) {
      ctx.fillText('X', 15, 30)
    }
    else if ( props.value === 'o' ) {
      ctx.fillText('O', 15, 30)
    }  
  }, [props.value])

  return(
    <canvas
      onClick={handleClick}
      className='square'
      ref={canvasRef}
      height={props.height}
      width={props.width}
      style={{backgroundColor: '#f1f1f1'}}
    />
  )
}

export default Square