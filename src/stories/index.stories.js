import React from 'react';
import { storiesOf } from '@storybook/react'
import Square from '../Square'
import Board from '../Board'

storiesOf('tictactoe game', module)
  .add('square', () =>  
    <Square
      width='50'
      height='50'
      value='o'
    />
  )
  .add('board', () =>  
    <Board      
      squares={
        [null, null, null, 
          null, null, null, 
        null, null, null]} 
    />
  )  